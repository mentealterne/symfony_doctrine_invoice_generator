<?php 
// src/Form/InvoiceType.php
namespace App\Form;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use App\Entity\Invoice;
use App\Entity\InvoiceItem;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('invoice_date')
        ->add('invoice_number')
        ->add('client', ChoiceType::class, [
           "choices" => ["Mario Rossi" => 1,
            "Maria Rossi"=> 2,
            "Mario Bianchi"=> 3]
        ])
        ->add('invoice_items', CollectionType::class, [
            'by_reference' => false,
            'entry_type' => InvoiceItemType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,

        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}