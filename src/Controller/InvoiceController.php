<?php // src/Controller/InvoiceController.php
namespace App\Controller;
use App\Entity\InvoiceItem;
use App\Entity\Invoice;
use App\Form\InvoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class InvoiceController extends AbstractController
{
    public function form(Request $request)
    {
        $invoice = new Invoice();

       
        $invoiceItem1 = new InvoiceItem();
        $invoice->getInvoiceItems()->add($invoiceItem1);
        

        $form = $this->createForm(InvoiceType::class, $invoice);
        $entityManager = $this->getDoctrine()->getManager();

        $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        
        $invoice = $form->getData();
         $entityManager->persist($invoice);
         foreach($invoice->getInvoiceItems() as $invoiceItem) {
             $invoiceItem->setGrossAmount($invoiceItem->getNetAmount() + $invoiceItem->getVatAmount());
             $invoiceItem->setInvoice($invoice);
             $entityManager->persist($invoiceItem);
         }
         $entityManager->flush();
         echo "Invoice added correctly into database";

    }

       

        return $this->render('invoice.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    public function success(Request $request) {
        $this->render('success_invoice_insertion.html.twig');
    }
}