<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // _twig_error_test
        if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
        }

        if (0 === strpos($pathinfo, '/api')) {
            // api_entrypoint
            if (preg_match('#^/api(?:/(?P<index>index)(?:\\.(?P<_format>[^/]++))?)?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'api_entrypoint']), array (  '_controller' => 'api_platform.action.entrypoint',  '_format' => '',  '_api_respond' => '1',  'index' => 'index',));
            }

            // api_doc
            if (0 === strpos($pathinfo, '/api/docs') && preg_match('#^/api/docs(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'api_doc']), array (  '_controller' => 'api_platform.action.documentation',  '_api_respond' => '1',  '_format' => '',));
            }

            // api_jsonld_context
            if (0 === strpos($pathinfo, '/api/contexts') && preg_match('#^/api/contexts/(?P<shortName>.+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'api_jsonld_context']), array (  '_controller' => 'api_platform.jsonld.action.context',  '_api_respond' => '1',  '_format' => 'jsonld',));
            }

            if (0 === strpos($pathinfo, '/api/invoice_items')) {
                // api_invoice_items_get_collection
                if (preg_match('#^/api/invoice_items(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoice_items_get_collection']), array (  '_controller' => 'api_platform.action.get_collection',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\InvoiceItem',  '_api_collection_operation_name' => 'get',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_api_invoice_items_get_collection;
                    }

                    return $ret;
                }
                not_api_invoice_items_get_collection:

                // api_invoice_items_post_collection
                if (preg_match('#^/api/invoice_items(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoice_items_post_collection']), array (  '_controller' => 'api_platform.action.post_collection',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\InvoiceItem',  '_api_collection_operation_name' => 'post',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_api_invoice_items_post_collection;
                    }

                    return $ret;
                }
                not_api_invoice_items_post_collection:

                // api_invoice_items_get_item
                if (preg_match('#^/api/invoice_items/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoice_items_get_item']), array (  '_controller' => 'api_platform.action.get_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\InvoiceItem',  '_api_item_operation_name' => 'get',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_api_invoice_items_get_item;
                    }

                    return $ret;
                }
                not_api_invoice_items_get_item:

                // api_invoice_items_delete_item
                if (preg_match('#^/api/invoice_items/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoice_items_delete_item']), array (  '_controller' => 'api_platform.action.delete_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\InvoiceItem',  '_api_item_operation_name' => 'delete',));
                    if (!in_array($requestMethod, ['DELETE'])) {
                        $allow = array_merge($allow, ['DELETE']);
                        goto not_api_invoice_items_delete_item;
                    }

                    return $ret;
                }
                not_api_invoice_items_delete_item:

                // api_invoice_items_put_item
                if (preg_match('#^/api/invoice_items/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoice_items_put_item']), array (  '_controller' => 'api_platform.action.put_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\InvoiceItem',  '_api_item_operation_name' => 'put',));
                    if (!in_array($requestMethod, ['PUT'])) {
                        $allow = array_merge($allow, ['PUT']);
                        goto not_api_invoice_items_put_item;
                    }

                    return $ret;
                }
                not_api_invoice_items_put_item:

            }

            elseif (0 === strpos($pathinfo, '/api/invoices')) {
                // api_invoices_get_collection
                if (preg_match('#^/api/invoices(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoices_get_collection']), array (  '_controller' => 'api_platform.action.get_collection',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\Invoice',  '_api_collection_operation_name' => 'get',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_api_invoices_get_collection;
                    }

                    return $ret;
                }
                not_api_invoices_get_collection:

                // api_invoices_post_collection
                if (preg_match('#^/api/invoices(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoices_post_collection']), array (  '_controller' => 'api_platform.action.post_collection',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\Invoice',  '_api_collection_operation_name' => 'post',));
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_api_invoices_post_collection;
                    }

                    return $ret;
                }
                not_api_invoices_post_collection:

                // api_invoices_get_item
                if (preg_match('#^/api/invoices/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoices_get_item']), array (  '_controller' => 'api_platform.action.get_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\Invoice',  '_api_item_operation_name' => 'get',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_api_invoices_get_item;
                    }

                    return $ret;
                }
                not_api_invoices_get_item:

                // api_invoices_delete_item
                if (preg_match('#^/api/invoices/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoices_delete_item']), array (  '_controller' => 'api_platform.action.delete_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\Invoice',  '_api_item_operation_name' => 'delete',));
                    if (!in_array($requestMethod, ['DELETE'])) {
                        $allow = array_merge($allow, ['DELETE']);
                        goto not_api_invoices_delete_item;
                    }

                    return $ret;
                }
                not_api_invoices_delete_item:

                // api_invoices_put_item
                if (preg_match('#^/api/invoices/(?P<id>[^/\\.]++)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'api_invoices_put_item']), array (  '_controller' => 'api_platform.action.put_item',  '_format' => NULL,  '_api_resource_class' => 'App\\Entity\\Invoice',  '_api_item_operation_name' => 'put',));
                    if (!in_array($requestMethod, ['PUT'])) {
                        $allow = array_merge($allow, ['PUT']);
                        goto not_api_invoices_put_item;
                    }

                    return $ret;
                }
                not_api_invoices_put_item:

            }

        }

        // app_invoice
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\InvoiceController::form',  '_route' => 'app_invoice',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_app_invoice;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'app_invoice'));
            }

            return $ret;
        }
        not_app_invoice:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
