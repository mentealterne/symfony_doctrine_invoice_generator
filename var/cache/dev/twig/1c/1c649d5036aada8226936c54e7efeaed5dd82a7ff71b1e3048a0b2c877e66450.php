<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* invoice.html.twig */
class __TwigTemplate_722ec4b57034879fdb0ec14c26b1d1e4cd95cb24ee29017ff4695889294541cb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "invoice.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "invoice.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Generate new invoice";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 5
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 11
        $this->loadTemplate("nav.html.twig", "invoice.html.twig", 11)->display($context);
        // line 12
        echo "


   <div class=\"flex flex-row\"> 
   <div class=\"container mx-auto\">
   <div class=\"flex-none w-full h-20 bg-teal-600 mt-2 text-left align-middle\"> 
   <h2 class=\"text-2xl text-white align-middle h-full uppercase p-5\">invoice </h2>
   </div>
   <div class=\"flex w-full flex-col bg-gray-200 shadow-lg h-auto\">
   ";
        // line 21
        if ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), 'errors')) {
            // line 22
            echo "   <div class=\"bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative flex-none w-full\" role=\"alert\">
  <strong class=\"font-bold\">Error in form validation</strong>
  <span class=\"block sm:inline\">";
            // line 24
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), 'errors');
            echo "</span>
  <span class=\"absolute top-0 bottom-0 right-0 px-4 py-3\">
    <svg class=\"fill-current h-6 w-6 text-red-500\" role=\"button\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><title>Close</title><path d=\"M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z\"/></svg>
  </span>
</div>
";
        }
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), 'form_start');
        echo "     
     <div class=\"flex flex-row\">   <div class=\"flex-none w-1/3\">
   <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "invoice_date", [], "any", false, false, false, 33), 'label');
        echo "
      </label>
  ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 35, $this->source); })()), "invoice_date", [], "any", false, false, false, 35), 'widget');
        echo "
  ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 36, $this->source); })()), "invoice_date", [], "any", false, false, false, 36), 'errors');
        echo "
  </div>
       <div class=\"flex-none w-1/3\">
   <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), "invoice_number", [], "any", false, false, false, 40), 'label');
        echo "
      </label>
    ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 42, $this->source); })()), "invoice_number", [], "any", false, false, false, 42), 'widget');
        echo "
      ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), "invoice_number", [], "any", false, false, false, 43), 'errors');
        echo "
    </div>



  <div class=\"flex-none w-1/3\">
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "client", [], "any", false, false, false, 50), 'label');
        echo "
      </label>
    ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 52, $this->source); })()), "client", [], "any", false, false, false, 52), 'widget');
        echo "
      ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 53, $this->source); })()), "client", [], "any", false, false, false, 53), 'errors');
        echo "


  </div>
  

   
   </div>
   <div class=\"flex-none w-full\"> 
  <h2 class=\" block text-gray-700 text-sm font-bold mb-2 mt-5\"> Invoice items </div> 
  <div class=\"flex flex-col invoice_items\" data-prototype='  
  <div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Quantity </label>
  <input type=\"number name=\"invoice[invoice_items][__name__][quantity]\">  </div>
  <div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Description </label>
  <textarea name=\"invoice[invoice_items][__name__][description]\"> </textarea>
</div>
<div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Net Amount </label>
  <input type=\"text name=\"invoice[invoice_items][__name__][net_amount]\"> 
</div>
<div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Vat Amount </label>
  <input type=\"text name=\"invoice[invoice_items][__name__][vat_amount]\"> 
</div>'> 
  ";
        // line 79
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 79, $this->source); })()), "invoice_items", [], "any", false, false, false, 79));
        foreach ($context['_seq'] as $context["_key"] => $context["invoice_item"]) {
            // line 80
            echo "  <div class=\"flex flex-row bg-gray-400 p-3 adda\"> 
   
  <div class=\"flex-grow\"> 
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >";
            // line 83
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "quantity", [], "any", false, false, false, 83), 'label');
            echo " </label>


          ";
            // line 86
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "quantity", [], "any", false, false, false, 86), 'widget');
            echo "
            ";
            // line 87
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "quantity", [], "any", false, false, false, 87), 'errors');
            echo "


  </div>
  
  <div class=\"flex-grow\"> 
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >";
            // line 93
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "description", [], "any", false, false, false, 93), 'label');
            echo " </label>


          ";
            // line 96
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "description", [], "any", false, false, false, 96), 'widget');
            echo "
                      ";
            // line 97
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "description", [], "any", false, false, false, 97), 'errors');
            echo "


  </div>
  <div class=\"flex-grow\"> 
     <label class=\"block text-gray-700 text-sm font-bold mb-2\" >";
            // line 102
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "net_amount", [], "any", false, false, false, 102), 'label');
            echo " </label>

          ";
            // line 104
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "net_amount", [], "any", false, false, false, 104), 'widget');
            echo "
                                ";
            // line 105
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "net_amount", [], "any", false, false, false, 105), 'errors');
            echo "


  </div>
   <div class=\"flex-grow\"> 
     <label class=\"block text-gray-700 text-sm font-bold mb-2\" >";
            // line 110
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "vat_amount", [], "any", false, false, false, 110), 'label');
            echo " </label>

          ";
            // line 112
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "vat_amount", [], "any", false, false, false, 112), 'widget');
            echo "
                                          ";
            // line 113
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["invoice_item"], "vat_amount", [], "any", false, false, false, 113), 'errors');
            echo "


  </div>
  </div>

  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "  </div>
   <button class=\"bg-teal-500 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded mt-2\" id=\"newInvoiceItemButton\" type=\"button\">
  New Invoice item
</button>
   </div>
   <button class=\"bg-green-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-2\">
  Save invoice
</button>
   </div>

";
        // line 130
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 130, $this->source); })()), 'form_end');
        echo "
    </div>
   </div>

   <script> 
   var \$collectionHolder;

// setup an \"add a tag\" link


jQuery(document).ready(function() {
    \$collectionHolder = \$('div.invoice_items');
    \$addInvoiceItemButton = \$(\"#newInvoiceItemButton\");


   
    \$collectionHolder.data('index', \$collectionHolder.find(':input').length);

    \$addInvoiceItemButton.on('click', function(e) {
        addInvoiceItemForm(\$collectionHolder, \$addInvoiceItemButton);
    });

    function addInvoiceItemForm(\$collectionHolder, \$addInvoiceItemButton) {
    var prototype = \$collectionHolder.data('prototype');

    var index = \$collectionHolder.data('index');

    var newForm = prototype;
   
    newForm = newForm.replace(/__name__/g, index);

    \$collectionHolder.data('index', index + 1);

    var \$newInvoiceItem = \$(`<div class=\"flex flex-row bg-gray-400 p-3 a\"> 
 </div>`).append(newForm);
    \$addInvoiceItemButton.before(\$newInvoiceItem);

    // adding the removal button
        addInvoiceItemDeleteLink(\$newInvoiceItem);

}
function addInvoiceItemDeleteLink(\$newInvoiceItem) {
    var \$removeFormButton = \$(`
    <div class=\"flex-grow\"> 
    <button class=\"bg-red-500 hover:bg-red-700 text-white font-bold  p-4 rounded\">Delete invoice item</button>
    </div>
`);
    \$newInvoiceItem.append(\$removeFormButton);

    \$removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        \$newInvoiceItem.remove();
    });
}
});
   </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "invoice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 130,  286 => 120,  273 => 113,  269 => 112,  264 => 110,  256 => 105,  252 => 104,  247 => 102,  239 => 97,  235 => 96,  229 => 93,  220 => 87,  216 => 86,  210 => 83,  205 => 80,  201 => 79,  172 => 53,  168 => 52,  163 => 50,  153 => 43,  149 => 42,  144 => 40,  137 => 36,  133 => 35,  128 => 33,  122 => 30,  113 => 24,  109 => 22,  107 => 21,  96 => 12,  94 => 11,  87 => 10,  74 => 5,  67 => 4,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %}Generate new invoice{% endblock %}
{% block head %}
    {{ parent() }}
    <style type=\"text/css\">
        .important { color: #336699; }
    </style>
{% endblock %}
{% block content %}
{% include 'nav.html.twig' %}



   <div class=\"flex flex-row\"> 
   <div class=\"container mx-auto\">
   <div class=\"flex-none w-full h-20 bg-teal-600 mt-2 text-left align-middle\"> 
   <h2 class=\"text-2xl text-white align-middle h-full uppercase p-5\">invoice </h2>
   </div>
   <div class=\"flex w-full flex-col bg-gray-200 shadow-lg h-auto\">
   {% if form_errors(form) %}
   <div class=\"bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative flex-none w-full\" role=\"alert\">
  <strong class=\"font-bold\">Error in form validation</strong>
  <span class=\"block sm:inline\">{{ form_errors(form) }}</span>
  <span class=\"absolute top-0 bottom-0 right-0 px-4 py-3\">
    <svg class=\"fill-current h-6 w-6 text-red-500\" role=\"button\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><title>Close</title><path d=\"M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z\"/></svg>
  </span>
</div>
{% endif %}
{{form_start(form)}}     
     <div class=\"flex flex-row\">   <div class=\"flex-none w-1/3\">
   <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        {{form_label(form.invoice_date)}}
      </label>
  {{form_widget(form.invoice_date)}}
  {{form_errors(form.invoice_date)}}
  </div>
       <div class=\"flex-none w-1/3\">
   <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        {{form_label(form.invoice_number)}}
      </label>
    {{form_widget(form.invoice_number)}}
      {{form_errors(form.invoice_number)}}
    </div>



  <div class=\"flex-none w-1/3\">
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >
        {{form_label(form.client)}}
      </label>
    {{form_widget(form.client)}}
      {{form_errors(form.client)}}


  </div>
  

   
   </div>
   <div class=\"flex-none w-full\"> 
  <h2 class=\" block text-gray-700 text-sm font-bold mb-2 mt-5\"> Invoice items </div> 
  <div class=\"flex flex-col invoice_items\" data-prototype='  
  <div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Quantity </label>
  <input type=\"number name=\"invoice[invoice_items][__name__][quantity]\">  </div>
  <div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Description </label>
  <textarea name=\"invoice[invoice_items][__name__][description]\"> </textarea>
</div>
<div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Net Amount </label>
  <input type=\"text name=\"invoice[invoice_items][__name__][net_amount]\"> 
</div>
<div class=\"flex-grow\"> 
  <label class=\"block text-gray-700 text-sm font-bold mb-2\" > Vat Amount </label>
  <input type=\"text name=\"invoice[invoice_items][__name__][vat_amount]\"> 
</div>'> 
  {% for invoice_item in form.invoice_items %}
  <div class=\"flex flex-row bg-gray-400 p-3 adda\"> 
   
  <div class=\"flex-grow\"> 
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >{{form_label(invoice_item.quantity)}} </label>


          {{form_widget(invoice_item.quantity)}}
            {{form_errors(invoice_item.quantity)}}


  </div>
  
  <div class=\"flex-grow\"> 
    <label class=\"block text-gray-700 text-sm font-bold mb-2\" >{{form_label(invoice_item.description)}} </label>


          {{form_widget(invoice_item.description)}}
                      {{form_errors(invoice_item.description)}}


  </div>
  <div class=\"flex-grow\"> 
     <label class=\"block text-gray-700 text-sm font-bold mb-2\" >{{form_label(invoice_item.net_amount)}} </label>

          {{form_widget(invoice_item.net_amount)}}
                                {{form_errors(invoice_item.net_amount)}}


  </div>
   <div class=\"flex-grow\"> 
     <label class=\"block text-gray-700 text-sm font-bold mb-2\" >{{form_label(invoice_item.vat_amount)}} </label>

          {{form_widget(invoice_item.vat_amount)}}
                                          {{form_errors(invoice_item.vat_amount)}}


  </div>
  </div>

  {% endfor %}
  </div>
   <button class=\"bg-teal-500 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded mt-2\" id=\"newInvoiceItemButton\" type=\"button\">
  New Invoice item
</button>
   </div>
   <button class=\"bg-green-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-2\">
  Save invoice
</button>
   </div>

{{form_end(form)}}
    </div>
   </div>

   <script> 
   var \$collectionHolder;

// setup an \"add a tag\" link


jQuery(document).ready(function() {
    \$collectionHolder = \$('div.invoice_items');
    \$addInvoiceItemButton = \$(\"#newInvoiceItemButton\");


   
    \$collectionHolder.data('index', \$collectionHolder.find(':input').length);

    \$addInvoiceItemButton.on('click', function(e) {
        addInvoiceItemForm(\$collectionHolder, \$addInvoiceItemButton);
    });

    function addInvoiceItemForm(\$collectionHolder, \$addInvoiceItemButton) {
    var prototype = \$collectionHolder.data('prototype');

    var index = \$collectionHolder.data('index');

    var newForm = prototype;
   
    newForm = newForm.replace(/__name__/g, index);

    \$collectionHolder.data('index', index + 1);

    var \$newInvoiceItem = \$(`<div class=\"flex flex-row bg-gray-400 p-3 a\"> 
 </div>`).append(newForm);
    \$addInvoiceItemButton.before(\$newInvoiceItem);

    // adding the removal button
        addInvoiceItemDeleteLink(\$newInvoiceItem);

}
function addInvoiceItemDeleteLink(\$newInvoiceItem) {
    var \$removeFormButton = \$(`
    <div class=\"flex-grow\"> 
    <button class=\"bg-red-500 hover:bg-red-700 text-white font-bold  p-4 rounded\">Delete invoice item</button>
    </div>
`);
    \$newInvoiceItem.append(\$removeFormButton);

    \$removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        \$newInvoiceItem.remove();
    });
}
});
   </script>
{% endblock %}", "invoice.html.twig", "/Users/alfredoguida/Desktop/projects/kritek/invoice_gen/templates/invoice.html.twig");
    }
}
